import-module MSOnline 
$msolcred=get-credential 
connect-msolservice -credential $msolcred 

#Ready "NewPassword.csv" including "UserId" and corresponding "NewPassword"
import-csv -Encoding UTF8 NewPassword.csv | Foreach-Object {
    $userid = $_.UserId
    $accountPassword = $_.NewPassword

    #"ForceChangePassword $Ture" will force users to change their password at the first time login.
    Set-MsolUserPassword -UserPrincipalName $userid -NewPassword $accountPassword -ForceChangePassword $False
}